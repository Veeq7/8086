#include "stdlib.h"
#include "stdio.h"
#include "string.h"

typedef unsigned char       u8;
typedef unsigned short      u16;
typedef unsigned long       u32;
typedef unsigned long long  u64;

typedef char       s8;
typedef short      s16;
typedef long       s32;
typedef long long  s64;

struct LoadedFile {
	size_t size;
	char* data;
};

LoadedFile load_entire_file(const char* filename) {
	FILE* file = 0;
	errno_t err = fopen_s(&file, filename, "rb");
	if (!file) {
		return {};
	}

	LoadedFile loaded_file = {};
	fseek(file, 0, SEEK_END);
	loaded_file.size = ftell(file);
	fseek(file, 0, SEEK_SET);

	loaded_file.data = (char*) malloc(loaded_file.size + 1);
	fread(loaded_file.data, 1, loaded_file.size, file);
	loaded_file.data[loaded_file.size] = 0;

	fclose(file);
	return loaded_file;
}

struct Parser {
	char* at;
	size_t size;
};

u8 read_u8(Parser* parser) {
	if (!parser->size) {
		return 0;
	}
	parser->size--;
	return *parser->at++;
}

u16 read_u16(Parser* parser) {
	u16 a = read_u8(parser);
	u16 b = read_u8(parser);
	return a | b << 8;
}

u8 read_bits(u8* value, u8 size) {
	u8 offset_value = *value >> (8 - size);
	u8 mask = (1 << size) - 1;
	*value <<= size;
	return offset_value & mask;
}

bool expect_bits(u8* value, u8 size, u8 expected_value) {
	u8 temp = *value;
	if (read_bits(&temp, size) == expected_value) {
		*value = temp;
		return true;
	}
	return false;
}

enum { RegType_Short, RegType_Wide, RegType_Count };
enum Register {	Register_Count = 8 };

const char* register_names[RegType_Count][Register_Count] = {
	{
		"al",
		"cl",
		"dl",
		"bl",
		"ah",
		"ch",
		"dh",
		"bh"
	},
	{
		"ax",
		"cx",
		"dx",
		"bx",
		"sp",
		"bp",
		"si",
		"di"
	},
};

const char* index_register_names[Register_Count] = {
	"bx + si",
	"bx + di",
	"bp + si",
	"bp + di",
	"si",
	"di",
	"bp",
	"bx"
};

enum OpcodeType {
	Opcode_None,

	Opcode_MovRegMemToFromReg,
	Opcode_MovImmToRegMem,
	Opcode_MovImmToReg,
	Opcode_MovMemToAcc,
	Opcode_MovAccToMem,

	Opcode_Count,
};

struct Opcode {
	u8 type;

	u8 reg_is_destination;
	u8 is_wide;

	u8 mod;
	u8 reg;
	u8 rm;

	u16 address;
	s16 displacement;
	u16 data;
};

Opcode read_opcode(Parser* parser) {
	Opcode opcode = {};

	u8 byte = read_u8(parser);
	if (expect_bits(&byte, 6, 34)) { // Register/memory to/from register
		opcode.type = Opcode_MovRegMemToFromReg;
		opcode.reg_is_destination = read_bits(&byte, 1);
		opcode.is_wide = read_bits(&byte, 1);

		byte = read_u8(parser);
		opcode.mod = read_bits(&byte, 2);
		opcode.reg = read_bits(&byte, 3);
		opcode.rm  = read_bits(&byte, 3);

		bool direct_memory = opcode.mod == 0 && opcode.rm == 6;
		if (opcode.mod == 2 || direct_memory) {
			opcode.displacement = read_u16(parser);
		} else if (opcode.mod == 1) {
			opcode.displacement = (s8)read_u8(parser);
		}
	} else if (expect_bits(&byte, 7, 99)) { // Immediate to register/memory
		opcode.type = Opcode_MovImmToRegMem;
		opcode.is_wide = read_bits(&byte, 1);

		byte = read_u8(parser);
		opcode.mod = read_bits(&byte, 2);
		opcode.reg = read_bits(&byte, 3); // always zero
		opcode.rm  = read_bits(&byte, 3);

		bool direct_memory = opcode.mod == 0 && opcode.rm == 6;
		if (opcode.mod == 2 || direct_memory) {
			opcode.displacement = read_u16(parser);
		} else if (opcode.mod == 1) {
			opcode.displacement = (s8)read_u8(parser);
		}
		opcode.data = opcode.is_wide ? read_u16(parser) : read_u8(parser);
	} else if (expect_bits(&byte, 4, 11)) { // Immediate to register
		opcode.type = Opcode_MovImmToReg;
		opcode.is_wide = read_bits(&byte, 1);
		opcode.reg = read_bits(&byte, 3);
		opcode.data = opcode.is_wide ? read_u16(parser) : read_u8(parser);
	} else if (expect_bits(&byte, 7, 80)) { // Memory to accumulator
		opcode.type = Opcode_MovMemToAcc;
		opcode.is_wide = read_bits(&byte, 1);
		opcode.address = read_u16(parser);
	} else if (expect_bits(&byte, 7, 81)) { // Accumulator to memory
		opcode.type = Opcode_MovAccToMem;
		opcode.is_wide = read_bits(&byte, 1);
		opcode.address = read_u16(parser);
	}

	return opcode;
}

void print_opcode(Opcode* opcode) {
	switch (opcode->type) {
		case Opcode_None: {
			printf("; invalid opcode\n");
		} break;
		case Opcode_MovRegMemToFromReg: {
			char operand1[32] = {};
			char operand2[32] = {};
			if (opcode->mod == 3) { // Register
				sprintf_s(operand1, "%s", register_names[opcode->is_wide][opcode->reg]);
				sprintf_s(operand2, "%s", register_names[opcode->is_wide][opcode->rm]);
			} else { // Memory
				bool direct_memory = opcode->mod == 0 && opcode->rm == 6;
				if (direct_memory) {
					sprintf_s(operand1, "%s", register_names[opcode->is_wide][opcode->reg]);
					sprintf_s(operand2, "[%u]", opcode->displacement);
				} else if (opcode->displacement) {
					sprintf_s(operand1, "%s", register_names[opcode->is_wide][opcode->reg]);
					sprintf_s(operand2, "[%s %c %u]", index_register_names[opcode->rm], opcode->displacement < 0 ? '-' : '+', abs(opcode->displacement));
				} else {
					sprintf_s(operand1, "%s", register_names[opcode->is_wide][opcode->reg]);
					sprintf_s(operand2, "[%s]", index_register_names[opcode->rm]);
				}
			}
			if (opcode->reg_is_destination) {
				printf("mov %s, %s\n", operand1, operand2);
			} else {
				printf("mov %s, %s\n", operand2, operand1);
			}
		} break;
		case Opcode_MovImmToRegMem: {
			if (opcode->mod == 3) { // Register
				printf("mov %s, %u\n", register_names[opcode->is_wide][opcode->rm], opcode->data);
			} else { // Memory
				bool direct_memory = opcode->mod == 0 && opcode->rm == 6;
				const char* data_type = opcode->is_wide ? "word" : "byte";
				if (direct_memory) {
					printf("mov [%u], %s %u\n", opcode->displacement, data_type, opcode->data);
				} else if (opcode->displacement) {
					printf("mov [%s %c %u], %s %u\n", index_register_names[opcode->rm], opcode->displacement < 0 ? '-' : '+', abs(opcode->displacement), data_type, opcode->data);
				} else {
					printf("mov [%s], %s %u\n", index_register_names[opcode->rm], data_type, opcode->data);
				}
			}
		} break;
		case Opcode_MovImmToReg: {
			printf("mov %s, %u\n", register_names[opcode->is_wide][opcode->reg], opcode->data);
		} break;
		case Opcode_MovMemToAcc: {
			printf("mov %s, [%u]\n", register_names[opcode->is_wide][0], opcode->address);
		} break;
		case Opcode_MovAccToMem: {
			printf("mov [%u], %s\n", opcode->address, register_names[opcode->is_wide][0]);
		} break;
		default: {
			printf("; unsupported opcode print\n");
		} break;
	}
}

void disassembly(const char* filename) {
	LoadedFile file = load_entire_file(filename);
	if (!file.data) {
		return;
	}

	printf("\n\t--- %s ---\n\n", filename);

	printf("bits 16\n");
	Parser parser = {};
	parser.at   = file.data;
	parser.size = file.size;
	while (parser.size) {
		Opcode opcode = read_opcode(&parser);
		print_opcode(&opcode);
	}

	free(file.data);
}

int main(int argc, char** argv) {
	disassembly("asm/listing_0037_single_register_mov");
	disassembly("asm/listing_0038_many_register_mov");
	disassembly("asm/listing_0039_more_movs");
	disassembly("asm/listing_0040_challenge_movs");
	return 0;
}